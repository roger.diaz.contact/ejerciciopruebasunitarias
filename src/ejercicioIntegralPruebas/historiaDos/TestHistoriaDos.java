package ejercicioIntegralPruebas.historiaDos;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ejercicioIntegralPruebas.historiaUno.historiaUno;

class TestHistoriaDos {

	@Test
	void test() {
		historiaDos h2 = new historiaDos();
		historiaUno h1 = new historiaUno();
		
		//1
				Elemento elementox=  new Elemento(
						0,
						"",
						"ALTA",
						"BASICO",
						"MTP",
						"ACTIVO"		
						);
				
				assertEquals("ERROR", h1.getEtiquetaToxicidad(h2.getGradoToxicidad(elementox)));
		
		//1
		Elemento elemento=  new Elemento(
				7,
				"10-20",
				"ALTA",
				"BASICO",
				"MTP",
				"ACTIVO"		
				);
		
		assertEquals("AZUL", h1.getEtiquetaToxicidad(h2.getGradoToxicidad(elemento)));
		
//		2
		Elemento elemento2=  new Elemento(
				7,
				"20-30",
				"BAJA",
				"ALCALINO",
				"HOMEOPATICO",
				"EXCIPIENTE"		
				);
		
		
//		assertEquals(7.57, h2.getGradoToxicidad(elemento2));
		assertEquals("AZUL", h1.getEtiquetaToxicidad(h2.getGradoToxicidad(elemento2)));
		
		
//		3
		Elemento elemento3=  new Elemento(
				7,
				"10-20",
				"MEDIA",
				"BASICO",
				"MTP",
				"ACTIVO"		
				);
		
//		assertEquals(6.43, h2.getGradoToxicidad(elemento3));
		assertEquals("AZUL", h1.getEtiquetaToxicidad(h2.getGradoToxicidad(elemento3)));
		
		
//		4
		Elemento elemento4=  new Elemento(
				6,
				"20-30",
				"MEDIA",
				"BASICO",
				"HOMEOPATICO",
				"ACTIVO"		
				);
		
//		assertEquals(8.4, h2.getGradoToxicidad(elemento4));
		assertEquals("AMARILLO", h1.getEtiquetaToxicidad(h2.getGradoToxicidad(elemento4)));
		
//		5
		Elemento elemento5=  new Elemento(
				6,
				"10-20",
				"ALTA",
				"ALCALINO",
				"MTP",
				"ACTIVO"		
				);
		
//		assertEquals(13.43, h2.getGradoToxicidad(elemento5));
		assertEquals("AMARILLO", h1.getEtiquetaToxicidad(h2.getGradoToxicidad(elemento5)));
		
//		6
		Elemento elemento6=  new Elemento(
				6,
				"10-20",
				"BAJA",
				"BASICO",
				"MTP",
				"EXCIPIENTE"		
				);
		
//		assertEquals(3.78, h2.getGradoToxicidad(elemento6));
		assertEquals("VERDE", h1.getEtiquetaToxicidad(h2.getGradoToxicidad(elemento6)));
		
//		7
		Elemento elemento7=  new Elemento(
				7,
				"10-20",
				"BAJA",
				"BASICO",
				"MTP",
				"EXCIPIENTE"		
				);
		
//		assertEquals(3.89, h2.getGradoToxicidad(elemento7));
		assertEquals("AZUL", h1.getEtiquetaToxicidad(h2.getGradoToxicidad(elemento7)));
		
//		8
		Elemento elemento8=  new Elemento(
				7,
				"10-20",
				"MEDIA",
				"ALCALINO",
				"MTP",
				"ACTIVO"		
				);
		
//		assertEquals(10.71, h2.getGradoToxicidad(elemento8));
		assertEquals("AMARILLO", h1.getEtiquetaToxicidad(h2.getGradoToxicidad(elemento8)));
		
//		9
		Elemento elemento9=  new Elemento(
				7,
				"20-30",
				"ALTA",
				"BASICO",
				"HOMEOPATICO",
				"ACTIVO"		
				);
		
//		assertEquals(10.6, h2.getGradoToxicidad(elemento9));
		assertEquals("AMARILLO", h1.getEtiquetaToxicidad(h2.getGradoToxicidad(elemento9)));
		
	}

}
