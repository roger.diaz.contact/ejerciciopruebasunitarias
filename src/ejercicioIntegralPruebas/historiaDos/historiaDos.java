package ejercicioIntegralPruebas.historiaDos;

import java.text.DecimalFormat;

public class historiaDos {
	private static DecimalFormat formato = new DecimalFormat("#.##");

	public Double getGradoToxicidad(Elemento elemento) {
		Double gradoToxicidad = -1d;

		elemento.setPondPH(this.evaluarPH(elemento.getPH()));
		if (elemento.getPondPH() != 0) {

			elemento.setPondNP(this.evaluarNP(elemento.getNP()));
			if (elemento.getPondNP() != 0) {

				elemento.setPondCN(this.evaluarCN(elemento.getCN()));
				if (elemento.getPondCN() != 0) {

					elemento.setPondAcidez(this.evaluarAcidez(elemento.getAcidez()));
					if (elemento.getPondAcidez() != 0) {

						elemento.setPondTipo(this.evaluarTipo(elemento.getTipo()));
						if (elemento.getPondTipo() != 0) {
							elemento.setPondComp(this.evaluarComp(elemento.getComposicion()));
							if (elemento.getPondComp() != 0) {
								return new Double(formato.format(this.calculoToxicidad(elemento)));
							}
						}
					}

				}

			}

		} 

		return gradoToxicidad;

	}

	private Double calculoToxicidad(Elemento elemento) {
		Double TX = 0d;

		Double phNp = (double) (elemento.getPondPH() + elemento.getPondNP());
		Double cnXa = new Double(elemento.getPondCN() * elemento.getPondAcidez());
		Double ct = (double) (elemento.getPondComp() + elemento.getPondTipo());

		TX = ((phNp) + (cnXa)) / ct;

		return TX;
	}

	private Integer evaluarComp(String composicion) {
		Integer res = 0;

		if (composicion.equals("ACTIVO")) {
			res = 2;
		}
		if (composicion.equals("EXCIPIENTE")) {
			res = 4;
		}

		return res;
	}

	public Integer evaluarPH(Integer ph) {
		Integer res = 0;
		if (ph == 7) {
			res = 7;
		}

		if (ph == 6) {
			res = 6;
		}

		return res;

	}

	public Integer evaluarNP(String np) {
		Integer res = 0;

		if (np.equals("10-20")) {
			res = 8;
		}

		if (np.equals("20-30")) {
			res = 6;
		}

		return res;
	}

	public Integer evaluarCN(String cn) {
		Integer res = 0;

		if (cn.equals("ALTA")) {
			res = 20;
		}

		if (cn.equals("BAJA")) {
			res = 10;
		}

		if (cn.equals("MEDIA")) {
			res = 15;
		}

		return res;
	}

	public Integer evaluarAcidez(String acidez) {
		Integer res = 0;

		if (acidez.equals("BASICO")) {
			res = 2;
		}

		if (acidez.equals("ALCALINO")) {
			res = 4;
		}

		return res;
	}

	public Integer evaluarTipo(String tipo) {
		Integer res = 0;

		if (tipo.equals("MTP")) {
			res = 5;
		}
		if (tipo.equals("HOMEOPATICO")) {
			res = 3;
		}

		return res;
	}

}
