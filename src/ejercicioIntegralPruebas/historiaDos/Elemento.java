package ejercicioIntegralPruebas.historiaDos;

public class Elemento {
	public Integer PH;
	public String NP;
	public String CN;
	public String acidez;
	public String tipo;
	public String composicion;
	
	Integer pondPH = 0, pondNP = 0, pondCN = 0, pondAcidez = 0, pondTipo = 0, pondComp = 0;
	
	
	public Integer getPondPH() {
		return pondPH;
	}


	public void setPondPH(Integer pondPH) {
		this.pondPH = pondPH;
	}


	public Integer getPondNP() {
		return pondNP;
	}


	public void setPondNP(Integer pondNP) {
		this.pondNP = pondNP;
	}


	public Integer getPondCN() {
		return pondCN;
	}


	public void setPondCN(Integer pondCN) {
		this.pondCN = pondCN;
	}


	public Integer getPondAcidez() {
		return pondAcidez;
	}


	public void setPondAcidez(Integer pondAcidez) {
		this.pondAcidez = pondAcidez;
	}


	public Integer getPondTipo() {
		return pondTipo;
	}


	public void setPondTipo(Integer pondTipo) {
		this.pondTipo = pondTipo;
	}


	public Integer getPondComp() {
		return pondComp;
	}


	public void setPondComp(Integer pondComp) {
		this.pondComp = pondComp;
	}


	public Elemento(Integer ph,String np,String cn,String acidez,String tipo,String composicion) {
		this.PH=ph;
		this.NP = np;
		this.CN= cn;
		this.acidez= acidez;
		this.tipo=tipo;
		this.composicion=composicion;
	}
	
	
	public Integer getPH() {
		return PH;
	}
	public void setPH(Integer pH) {
		PH = pH;
	}
	public String getNP() {
		return NP;
	}
	public void setNP(String nP) {
		NP = nP;
	}
	public String getCN() {
		return CN;
	}
	public void setCN(String cN) {
		CN = cN;
	}
	public String getAcidez() {
		return acidez;
	}
	public void setAcidez(String acidez) {
		this.acidez = acidez;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getComposicion() {
		return composicion;
	}
	public void setComposicion(String composicion) {
		this.composicion = composicion;
	}
	
	
	
	
	
	
}
