package ejercicioIntegralPruebas.historiaUno;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestHistoriaUno {

	
	@Test
	void testError() {
		historiaUno h1 = new historiaUno();		
		assertEquals("ERROR", h1.getEtiquetaToxicidad(-0.5d));
		assertEquals("ERROR", h1.getEtiquetaToxicidad(-100d));
	}
	
	@Test
	void testVerde() {
		historiaUno h1 = new historiaUno();		
		assertEquals("VERDE", h1.getEtiquetaToxicidad(0d));
		assertEquals("VERDE", h1.getEtiquetaToxicidad(3.8));
	}
	
	
	@Test
	void testAzul() {
		historiaUno h1 = new historiaUno();		
		assertEquals("AZUL", h1.getEtiquetaToxicidad(3.81));
		assertEquals("AZUL", h1.getEtiquetaToxicidad(8.25));
	}
	
	@Test
	void testAmarillo() {
		historiaUno h1 = new historiaUno();		
		assertEquals("AMARILLO", h1.getEtiquetaToxicidad(8.26));
		assertEquals("AMARILLO", h1.getEtiquetaToxicidad(15d));
	}
	
	
	@Test
	void testRojo() {
		historiaUno h1 = new historiaUno();		
		assertEquals("ROJO", h1.getEtiquetaToxicidad(15.1d));
		assertEquals("ROJO", h1.getEtiquetaToxicidad(200d));
	}
	
	


}
