package ejercicioIntegralPruebas.historiaUno;

public class historiaUno {

	public static Double etiquetaVerde = 3.8;
	public static Double etiquetaAzul = 3.81;
	public static Double etiquetaAmarillo = 8.26;
	public static Double etiquetaRojo = 15d;

	public String getEtiquetaToxicidad(Double cantToxicidad) {
		String resultado = "ERROR";

		if (cantToxicidad >=0 && cantToxicidad <=etiquetaVerde) {

			resultado = "VERDE";

		}

		if (cantToxicidad >= etiquetaAzul && cantToxicidad <= etiquetaAmarillo) {

			resultado = "AZUL";

		}

		if (cantToxicidad >= etiquetaAmarillo && cantToxicidad <= etiquetaRojo) {

			resultado = "AMARILLO";

		}

		if (cantToxicidad > etiquetaRojo ) {

			resultado = "ROJO";

		}

		System.out.println("ETIQUETA :"+resultado+" para el grado de toxicidad :"+cantToxicidad);
		return resultado;
	}

}
